from pdkmaster.design import layout as lay, library as lbry

__all__ = ["SPSRAMLibrary"]


class _SPCell(lbry._Cell):
    def __init__(self, lib):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        self.lib = lib

        super().__init__(lib, "SPCell")

        self._add_circuit()
        self._add_layout()

    def _add_circuit(self):
        circuit = self.new_circuit()

    def _add_layout(self):
        layouter = self.new_circuitlayouter()
        layout = self.layout
        cell_width = 2.64
        cell_height = 3.84
        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)


class _Column64X2X8(lbry._Cell):
    def __init__(self, lib, cell):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(cell, _SPCell), "Internal error"
        self.lib = lib
        self.cell = cell

        super().__init__(lib, "Column64X2X8")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        # 8 d & q
        for i in range(8):
            circuit.new_net(f"d[{i}]", external=True)
            circuit.new_net(f"q[{i}]", external=True)
        circuit.new_net(f"we", external=True)

    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout

        spcell_bounds = self.cell.layout.boundary
        assert spcell_bounds.left == 0.0
        spcell_width = spcell_bounds.right
        assert spcell_bounds.bottom == 0.0
        spcell_height = spcell_bounds.top
        cell_width = 16*spcell_width
        cell_height = 100 + 64*spcell_height
        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)

        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065

        # Eight q outputs
        for i in range(8):
            x = (2*i + 0.5)*spcell_width
            layouter.add_wire(
                net=nets[f"q[{i}]"], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )

        # Eight d inputs + we
        for n, pin in (
            (1, "d[3]"), (3, "d[2]"), (5, "d[1]"), (7, "d[0]"),
            (9, "d[7]"), (11, "d[6]"), (13, "d[5]"), (15, "d[4]"),
            (2.5, "we"),
        ):
            x = (n + 0.5)*spcell_width
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )


class _Column128X4X8(lbry._Cell):
    def __init__(self, lib, cell):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(cell, _SPCell), "Internal error"
        self.lib = lib
        self.cell = cell

        super().__init__(lib, "Column128X4X8")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        # 8 d & q
        for i in range(8):
            circuit.new_net(f"d[{i}]", external=True)
            circuit.new_net(f"q[{i}]", external=True)
        circuit.new_net(f"we", external=True)

    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout

        spcell_bounds = self.cell.layout.boundary
        assert spcell_bounds.left == 0.0, "Internal error"
        spcell_width = spcell_bounds.right
        assert spcell_bounds.bottom == 0.0, "Internal error"
        spcell_height = spcell_bounds.top
        cell_width = 32*spcell_width
        cell_height = 80 + 128*spcell_height
        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)

        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065

        # Eight q outputs
        for i in range(8):
            x = (4*i + 0.5)*spcell_width
            layouter.add_wire(
                net=nets[f"q[{i}]"], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )

        # Eight d inputs + we
        for n, pin in (
            (2, "d[1]"), (6, "d[0]"), (10, "d[3]"), (14, "d[2]"),
            (18, "d[5]"), (22, "d[4]"), (26, "d[7]"), (30, "d[6]"),
            (3, "we"),
        ):
            x = (n + 0.5)*spcell_width
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )


class _Periphery64X2(lbry._Cell):
    def __init__(self, lib, height):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(height, float), "Internal error"
        self.lib = lib
        self.height = height

        super().__init__(lib, "Periphery64X2")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        circuit.new_net("clk", external=True)
        for i in range(7):
            circuit.new_net(f"a[{i}]", external=True)
        
    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout

        cell_height = self.height

        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065
        pins = ("clk", *(f"a[{i}]" for i in range(7)))
        for i, pin in enumerate(pins):
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=(i + 1)*pin_pitch, width=pin_width, y=0.5*pin_height, height=pin_height,
            )
        cell_width = (len(pins) + 2)*pin_pitch

        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)


class _Periphery128X4(lbry._Cell):
    def __init__(self, lib, height):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(height, float), "Internal error"
        self.lib = lib
        self.height = height

        super().__init__(lib, "Periphery128X4")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        circuit.new_net("clk", external=True)
        for i in range(9):
            circuit.new_net(f"a[{i}]", external=True)
        
    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout

        cell_height = self.height

        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065
        pins = ("clk", *(f"a[{i}]" for i in range(9)))
        for i, pin in enumerate(pins):
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=(i + 1)*pin_pitch, width=pin_width, y=0.5*pin_height, height=pin_height,
            )
        cell_width = (len(pins) + 2)*pin_pitch

        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)


class _Block64X2X8X8(lbry._Cell):
    def __init__(self, lib, peri, col):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(peri, _Periphery64X2), "Internal error"
        assert isinstance(col, _Column64X2X8), "Internal error"
        self.lib = lib
        self.peri = peri
        self.col = col

        super().__init__(lib, "Block64X2X8X8")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        # TODO: add vdd/vss
        circuit.new_net("clk", external=True)
        for i in range(7):
            circuit.new_net(f"a[{i}]", external=True)
        for i in range(64):
            circuit.new_net(f"d[{i}]", external=True)
            circuit.new_net(f"q[{i}]", external=True)
        for i in range(8):
            circuit.new_net(f"we[{i}]", external=True)

    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout
        peri_bounds = self.peri.layout.boundary
        col_bounds = self.col.layout.boundary
        spcell_bounds = self.col.cell.layout.boundary
        assert peri_bounds.top == col_bounds.top, "Internal error"
        cell_width = peri_bounds.right + 8*col_bounds.right
        cell_height = peri_bounds.top
        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)

        assert peri_bounds.left == 0.0, "Internal error"
        peri_width = peri_bounds.right
        assert col_bounds.left == 0.0, "Internal error"
        col_width = col_bounds.right
        assert spcell_bounds.left == 0.0, "Internal error"
        spcell_width = spcell_bounds.right
        
        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065

        pins = ("clk", *(f"a[{i}]" for i in range(7)))
        for i, pin in enumerate(pins):
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=(i + 1)*pin_pitch, width=pin_width, y=0.5*pin_height, height=pin_height,
            )
 
        # Eight by eight q outputs
        for i in range(8):
            x_i = (2*i + 0.5)*spcell_width
            for j in range(8):
                n = j*8 + i
                x = peri_width + j*col_width + x_i
                layouter.add_wire(
                    net=nets[f"q[{n}]"], wire=pin_layer, pin=pin_layer.pin[0],
                    x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
                )

        # Eight by eight d inputs
        for i, n_i in (
            (3, 1), (2, 3), (1, 5), (0, 7), (7, 9), (6, 11), (5, 13), (4, 15),
        ):
            x_i = (n_i + 0.5)*spcell_width
            for j in range(8):
                n = j*8 + i
                x = peri_width + j*col_width + x_i
                layouter.add_wire(
                    net=nets[f"d[{n}]"], wire=pin_layer, pin=pin_layer.pin[0],
                    x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
                )

        # Eight we inputs
        for i in range(8):
            x = peri_width + i*col_width + 3*spcell_width
            layouter.add_wire(
                net=nets[f"we[{i}]"], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )


class _Block128X4X8X8(lbry._Cell):
    def __init__(self, lib, peri, col):
        assert isinstance(lib, SPSRAMLibrary), "Internal error"
        assert isinstance(peri, _Periphery128X4), "Internal error"
        assert isinstance(col, _Column128X4X8), "Internal error"
        self.lib = lib
        self.peri = peri
        self.col = col

        super().__init__(lib, "Block128X4X8X8")

        self._add_circuit()
        self._add_layout()

    @property
    def tech(self):
        return self.lib.tech

    def _add_circuit(self):
        circuit = self.new_circuit()

        # TODO: add vdd/vss
        circuit.new_net("clk", external=True)
        for i in range(9):
            circuit.new_net(f"a[{i}]", external=True)
        for i in range(64):
            circuit.new_net(f"d[{i}]", external=True)
            circuit.new_net(f"q[{i}]", external=True)
        for i in range(8):
            circuit.new_net(f"we[{i}]", external=True)

    def _add_layout(self):
        nets = self.circuit.nets

        layouter = self.new_circuitlayouter()
        layout = self.layout
        peri_bounds = self.peri.layout.boundary
        col_bounds = self.col.layout.boundary
        spcell_bounds = self.col.cell.layout.boundary
        assert peri_bounds.top == col_bounds.top, "Internal error"
        cell_width = peri_bounds.right + 8*col_bounds.right
        cell_height = peri_bounds.top
        layout.boundary = lay.Rect(0, 0, cell_width, cell_height)

        assert peri_bounds.left == 0.0, "Internal error"
        peri_width = peri_bounds.right
        assert col_bounds.left == 0.0, "Internal error"
        col_width = col_bounds.right
        assert spcell_bounds.left == 0.0, "Internal error"
        spcell_width = spcell_bounds.right
        
        pin_layer = self.tech.primitives.METAL2
        pin_width = self.tech.computed.min_width(pin_layer, up=True, down=True)
        pin_height = 1.0
        pin_pitch = 20*0.065

        pins = ("clk", *(f"a[{i}]" for i in range(9)))
        for i, pin in enumerate(pins):
            layouter.add_wire(
                net=nets[pin], wire=pin_layer, pin=pin_layer.pin[0],
                x=(i + 1)*pin_pitch, width=pin_width, y=0.5*pin_height, height=pin_height,
            )
 
        # Eight by eight q outputs
        for i in range(8):
            x_i = (4*i + 0.5)*spcell_width
            for j in range(8):
                n = j*8 + i
                x = peri_width + j*col_width + x_i
                layouter.add_wire(
                    net=nets[f"q[{n}]"], wire=pin_layer, pin=pin_layer.pin[0],
                    x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
                )

        # Eight by eight d inputs
        for i, n_i in (
            (1, 2), (0, 6), (3, 10), (2, 14), (5, 18), (4, 22), (7, 26), (6, 30),
        ):
            x_i = (n_i + 0.5)*spcell_width
            for j in range(8):
                n = j*8 + i
                x = peri_width + j*col_width + x_i
                layouter.add_wire(
                    net=nets[f"d[{n}]"], wire=pin_layer, pin=pin_layer.pin[0],
                    x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
                )

        # Eight we inputs
        for i in range(8):
            x = peri_width + i*col_width + 3.5*spcell_width
            layouter.add_wire(
                net=nets[f"we[{i}]"], wire=pin_layer, pin=pin_layer.pin[0],
                x=x, width=pin_width, y=0.5*pin_height, height=pin_height,
            )


class SPSRAMLibrary(lbry.Library):
    def __init__(self, name, *, tech, cktfab, layoutfab):
        super().__init__(name, tech=tech, cktfab=cktfab, layoutfab=layoutfab)

        cell = _SPCell(self)
        col1 = _Column64X2X8(self, cell)
        peri1 = _Periphery64X2(self, col1.layout.boundary.top)
        block1 = _Block64X2X8X8(self, peri1, col1)
        col2 = _Column128X4X8(self, cell)
        peri2 = _Periphery128X4(self, col2.layout.boundary.top)
        block2 = _Block128X4X8X8(self, peri2, col2)
        self.cells += (cell, col1, peri1, block1, col2, peri2, block2)
        